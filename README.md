# YOLOv3-adv


## Introduction
This Python script implements the "disappearance attack" and the 
"creation attack" described this paper:
[Physical Adversarial Examples for Object Detectors](https://www.usenix.org/system/files/conference/woot18/woot18-paper-eykholt.pdf).

The original detection script is from [Ultralytics](https://github.com/ultralytics/yolov3),
a [YOLOv3](https://pjreddie.com/darknet/yolo/) implementation using the 
[COCO dataset](https://cocodataset.org/).

## Usage
```bash
python3 detect.py -h
usage: detect.py [-h] [--weights WEIGHTS [WEIGHTS ...]] [--source SOURCE] [--img-size IMG_SIZE] [--conf-thres CONF_THRES] [--iou-thres IOU_THRES] [--max-det MAX_DET] [--device DEVICE] [--view-img] [--save-txt] [--save-conf]
                 [--save-crop] [--nosave] [--classes CLASSES [CLASSES ...]] [--agnostic-nms] [--augment] [--update] [--project PROJECT] [--name NAME] [--exist-ok] [--line-thickness LINE_THICKNESS] [--hide-labels] [--hide-conf] [--adv]
                 [--adv-attack ADV_ATTACK] [--adv-lambda ADV_LAMBDA] [--adv-max-epoch ADV_MAX_EPOCH] [--adv-class ADV_CLASS] [--adv-box-x1 ADV_BOX_X1] [--adv-box-y1 ADV_BOX_Y1] [--adv-box-x2 ADV_BOX_X2] [--adv-box-y2 ADV_BOX_Y2]
                 [--adv-sticker-width ADV_STICKER_WIDTH] [--adv-sticker-height ADV_STICKER_HEIGHT] [--adv-n-transf ADV_N_TRANSF] [--adv-learning-rate ADV_LEARNING_RATE] [--adv-out-file ADV_OUT_FILE]

optional arguments:
  -h, --help            show this help message and exit
  --weights WEIGHTS [WEIGHTS ...]
                        model.pt path(s)
  --source SOURCE       source
  --img-size IMG_SIZE   inference size (pixels)
  --conf-thres CONF_THRES
                        object confidence threshold
  --iou-thres IOU_THRES
                        IOU threshold for NMS
  --max-det MAX_DET     maximum number of detections per image
  --device DEVICE       cuda device, i.e. 0 or 0,1,2,3 or cpu
  --view-img            display results
  --save-txt            save results to *.txt
  --save-conf           save confidences in --save-txt labels
  --save-crop           save cropped prediction boxes
  --nosave              do not save images/videos
  --classes CLASSES [CLASSES ...]
                        filter by class: --class 0, or --class 0 2 3
  --agnostic-nms        class-agnostic NMS
  --augment             augmented inference
  --update              update all models
  --project PROJECT     save results to project/name
  --name NAME           save results to project/name
  --exist-ok            existing project/name ok, do not increment
  --line-thickness LINE_THICKNESS
                        bounding box thickness (pixels)
  --hide-labels         hide labels
  --hide-conf           hide confidences
  --adv                 performs adversarial attack
  --adv-attack ADV_ATTACK
                        0 = disappearance attack - 1 = creation attack
  --adv-lambda ADV_LAMBDA
                        lambda for the attack
  --adv-max-epoch ADV_MAX_EPOCH
                        max number of epoch for the attack
  --adv-class ADV_CLASS
                        class for the attack (COCO dataset)
  --adv-box-x1 ADV_BOX_X1
                        upper X for adversarial box 
  --adv-box-y1 ADV_BOX_Y1
                        upper Y for adversarial box 
  --adv-box-x2 ADV_BOX_X2
                        lower X for adversarial box 
  --adv-box-y2 ADV_BOX_Y2
                        lower Y for adversarial box 
  --adv-sticker-width ADV_STICKER_WIDTH
                        width of the sticker (creation only)
  --adv-sticker-height ADV_STICKER_HEIGHT
                        Height of the sticker (creation only)
  --adv-n-transf ADV_N_TRANSF
                        number of transformations in every epoch
  --adv-learning-rate ADV_LEARNING_RATE
                        optimizer learning rate
  --adv-out-file ADV_OUT_FILE
                        lambda for the attack
  --print-classes       prints number->class (COCO DATASET)
```

Default values are ready a disappearance attack on the "bottle.jpg" image included.
```bash
python3 detect.py --adv
```

The script will generate the image attack.bmp and a perturbed image
in the same directory of the script. The perturbed image is to be used as 
a reference to where apply the attack image in the physical object.
It's possible to alter the parameter in order to change the experiment. 
The box coordinates are relative to the original image.

The creation attack will only generate the sticker.

## Examples
### Disapperance attack
```bash
 python detect.py --source grumpy.jpg --adv --adv-class 15 --adv-box-x1 200 --adv-box-y1 720 --adv-box-x2 500 --adv-box-y2 1050 --adv-max-epoch 1000
```
This will generate a disappearance attack, for class 15 (cat), using the box
(200,720) x (500x1050) using 1000 max epochs. 

### Creation attack
```bash
 python detect.py --adv --adv-attack 1 --adv-n-transf 0 --adv-max-epoch 1000 --adv-learning-rate 0.005 --adv-sticker-height 60 --adv-sticker-width 60 --adv-class 15

```
This will generate a creation attack, for class 15 (cat), for a 60x60 sticker 
usign 1000 max epochs, no image transformations and an optimizer learning rate
of 0.005.


## Files

Variable initialization and attack loop are defined in the original "detect.py" file.
Some modifications has been done to the original network because it wasn't retaining gradients ()

Attacks are defined in the "attack.py", and the losses computation in the "loss.py" file.
The file "utils.py" contains various functions to support the attack.

The "experiments" directory shows the numerical output of the string of some 
successful and unsuccessful attacks.