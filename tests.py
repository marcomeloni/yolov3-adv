import numpy as np
import cv2
import torch
from adv.attack import *

# img0 = cv2.imread("/home/marco/Pictures/Cat03.jpg")
# t_img0 = torch.Tensor(img0)
# #
# size = t_img0.size()
# mask = torch.zeros(size[:2])
# mask = mask.to(torch.uint8)
# mask = torch.zeros(size[:2], out=None, dtype="uint8")
# mask = np.zeros(size[:2])
# #
#cv2.rectangle(mask.numpy(), (300, 400), (1000, 800), 255, -1)
# masked_image = cv2.bitwise_and(img0, img0, mask=mask.numpy())
# masked_image_inv = 255 - masked_image
# print(masked_image_inv)
#
# row, col, ch = img0.shape
# mean = 0
# var = 4
# sigma = var**2  # 0.5
# gauss = np.random.normal(mean, sigma, (row, col, ch))
# gauss = gauss.reshape((row, col, ch))
#
# noised = img0 + gauss
#
# cv2.imwrite("/home/marco/Pictures/Cat03Mask.jpg", noised)

# print(torch.abs(a-b))
# print(torch.roll(a, -1, 0)) # rows
# print(torch.roll(a, -1, 1)) # cols
#
# print(torch.abs(torch.roll(a, -1, 0) - a))
# print(torch.abs(torch.roll(a, -1, 1) - a))

# result = torch.sum(torch.abs(torch.abs(torch.roll(a, -1, 1) - a) - torch.abs(torch.roll(a, -1, 0) - a)))
# print(result)

#print(tv(t_img0, 0, 0, 0, 0, 0))

# a = torch.Tensor(([[ 1.31417e+02,  2.40508e+02,  2.04613e+02,  5.11208e+02,  3.95980e-01,  0.00000e+00],
#         [ 3.96113e+02,  2.32377e+02,  4.79560e+02,  5.23199e+02,  8.49882e-01,  0.00000e+00],
#         [ 6.88396e+00,  1.34199e+02,  4.64892e+02,  4.35609e+02,  6.50158e-01,  5.00000e+00],
#         [ 2.72827e+01,  2.35812e+02,  1.45377e+02,  5.36758e+02,  6.14222e-01,  0.00000e+00],
#         [-9.73091e-02,  2.75544e+02,  4.49971e+01,  5.88516e+02,  2.54479e-01,  0.00000e+00]]))
#
# a.requires_grad = True
# print(a)
# print(a[:, 5] != 5)
# print(a[:, 4])
# print(torch.max(a[:, 4]))
# print(torch.max(a[a[:, 5] == 5, 4]))

# max_ind = torch.max(a, dim=0)
# print(max_ind.values[4])
# print(a[max_ind.indices[4]][5])

# m = torch.arange(48).view(3, 4, 4)
# m = torch.ones((3, 640, 480))
# print(m)
# # a = torch.arange(50, 98).view(3, 4, 4)
# # transposed = torch.transpose(masked_noise, 0, 2)
# # transposed = torch.transpose(transposed, 0, 1)
# a = torch.abs(torch.diff(m, dim=1))
# b = torch.abs(torch.diff(m, dim=2))
# # b = torch.transpose(b, 1, 2)
# print(a)
# print(b)
# print(a.shape)
# print(b.shape)
# print(a[:, :, 0:a.shape[2] - 1].shape)
# print(b[:, 0:b.shape[1] - 1, :].shape)
# print(torch.sum(torch.abs(a[:, :, 0:a.shape[2] - 1] -
#                           b[:, 0:b.shape[1] - 1, :])))

# r = [[2, 2], [3, 3]]
# a = np.ones((3, 600, 600))
# a = torch.zeros((3, 600, 600))
# a = torch.rand((3, 600, 600)) * 255
# triplets = get_print_triplets(600, 600)
# print(get_nps_op(a, triplets))
#
# blu = torch.zeros(noise_conv.shape)
# blu[2, :, :] = noise_conv.detach()[2]
# blu = blu.unsqueeze(0)
# write_image(blu,
#             'noiseB' + str(epoch) + '.bmp')
#
# green = torch.zeros(noise_conv.shape)
# green[1, :, :] = noise_conv.detach()[1]
# green = green.unsqueeze(0)
# write_image(green,
#             'noiseG' + str(epoch) + '.bmp')
#
# red = torch.zeros(noise_conv.shape)
# red[0, :, :] = noise_conv.detach()[0]
# red = red.unsqueeze(0)
# write_image(red,
#             'noiseR' + str(epoch) + '.bmp')

# np.savetxt('noiseR' + str(epoch) + '.txt', noise.detach().numpy()[0] * 255)
# np.savetxt('noiseG' + str(epoch) + '.txt', noise.detach().numpy()[1] * 255)
# np.savetxt('noiseB' + str(epoch) + '.txt', noise.detach().numpy()[2] * 255)

# np.savetxt('noiseCB' + str(epoch) + '.txt', noise_conv.detach().numpy()[0] * 255)
# np.savetxt('noiseCG' + str(epoch) + '.txt', noise_conv.detach().numpy()[1] * 255)
# np.savetxt('noiseCR' + str(epoch) + '.txt', noise_conv.detach().numpy()[2] * 255)


# print(torch.zeros(10, dtype=torch.long).fill_(2))
#
#
# def noise_forward_test(noise, perturbated_img,  x, y, adv_dims, n=1000):
#     # NORM_MEAN = [0.49139968, 0.48215827, 0.44653124]
#     # NORM_STD = [0.24703233, 0.24348505, 0.26158768]
#     for i in range(n):
#         m1 = random.uniform(0.481, 0.501)
#         m2 = random.uniform(0.472, 0.492)
#         m3 = random.uniform(0.436, 0.456)
#         n1 = random.uniform(0.237, 0.257)
#         n2 = random.uniform(0.233, 0.253)
#         n3 = random.uniform(0.251, 0.271)
#         nm = [m1, m2, m3]
#         ns = [n1, n2, n3]
#
#         p1 = str(round(m1, 5) * 10000)
#         p2 = str(round(m2, 5) * 10000)
#         p3 = str(round(m3, 5) * 10000)
#         p4 = str(round(n1, 5) * 10000)
#         p5 = str(round(n2, 5) * 10000)
#         p6 = str(round(n3, 5) * 10000)
#         filename = p1 + '-' + p2 + '-' + p3 + '-' + p4 + '-' + p5 + '-' + \
#                    p6 + '.jpg'
#
#         tm, ts = torch.FloatTensor(nm)[:, None, None], \
#                  torch.FloatTensor(ns)[:, None, None]
#         # Map noise values from [-infty,infty] to COCO min and max
#         noise_d = noise.clone().detach()
#         noise_d = (torch.tanh(noise_d) + 1 - 2 * tm) / (2 * ts)
#         # perturbated_img[0, :, y: y + adv_dims[1], x: x + adv_dims[0]] = noise_d
#         from torchvision import transforms
#         p = transforms.Compose([transforms.Scale(640)])
#         write_image(p(noise_d).unsqueeze(0), '/home/marco/Pictures/run/' + filename)
#
#     return noise

# starting_noise = torch.zeros(1, 3, adv_sticker_width,
#                                          adv_sticker_height,
#                                          dtype=torch.float64)
#             starting_noise = starting_noise + torch.randn(1, 3,
#                                                           adv_sticker_height,
#                                                           adv_sticker_width)
#
#             x, y = adv_coords[0], adv_coords[1]
#             perturbated_img = img.clone().detach()
#             perturbated_img[0, :, y: y + adv_sticker_height,
#                             x: x + adv_sticker_width] = starting_noise
#
#             write_image(perturbated_img, 'starting_noise.jpg')

a = torch.zeros((4, 4))
a[1:3, 1:3] = 1
print(a)

b = torch.rand((1, 3, 5, 5))
print(len(b.shape))
mask = create_mask(b, (1, 1, 2, 3))
print(mask)

