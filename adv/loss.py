import torch


# loss for disapeeareance attack
def loss_disappearance(pred, orig_class):
    predictions = pred[0]
    if predictions.shape[0] >= 1:
        p = predictions[predictions[:, 5] == orig_class, 4]
        if torch.all(p == False):
            loss = torch.zeros(1)[0]
        else:
            loss = torch.max(p)
    else:
        loss = torch.zeros(1)[0]
    return loss


# alternative loss that doesn't use nms
def loss_disappearance_no_nms(pred, orig_class):
    predictions = pred[0]
    if predictions.shape[0] >= 1:
        p = predictions[:, orig_class + 5]
        if p.shape[0] == 0:
            loss = torch.zeros(1)[0]
        else:
            index = torch.argmax(predictions[:, 4])
            loss = predictions[index, 4] * p[index]
    else:
        loss = torch.zeros(1)[0]
    return loss


class LossCreation:

    def __init__(self):
        self._index = 0
        self._last_prob = 0

    # loss for creation attack
    def loss_creation(self, pred, new_class, threshold=0.2):
        predictions = pred[0]
        if (self._index > 0) and (self._last_prob <= 0.8):
            prob_box = predictions[self._index, 4].clone().detach()
            class_max = torch.max(predictions[self._index, 4:]).\
                clone().detach()
            prob_new_class = predictions[self._index, new_class + 5]
            box = predictions[self._index, :4]
            print("Coords: " + str(box))
            print("Object prob: " + str(prob_box))
            print("Class prob: " + str(prob_new_class))
            print("Class max: " + str(class_max))
            loss = prob_box + (1 - prob_box) * prob_new_class
            self._last_prob = prob_new_class
        else:
            prob_box = torch.max(predictions[:, 4])
            index = torch.argmax(predictions[:, 4]).clone().detach()
            if prob_box > threshold:
                self._index = index
                self._last_prob = 0
            loss = prob_box
        return -loss

