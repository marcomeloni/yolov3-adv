from adv.utils import *


class PhyAttack:

    def __init__(self, best_object_function=99999999):
        self._best_object_function = best_object_function

    def attack_disappearance(self, model, loss_funct, img, noise, device,
                             lambda_attack, epoch, optimizer, attack_class,
                             transformations, triplets, opt,
                             adv_out_file, adv_coords, adv_dims, mask,
                             plot_data):
        generated_new = False
        # copy noise on top of img at right coordinates
        x, y = adv_coords[0], adv_coords[1]
        perturbated_img = img.clone().detach() * ~mask
        noise_conv = noise_forward(noise)
        masked_noise = (noise_conv * mask)
        perturbated_img = perturbated_img + masked_noise
        noise_only = perturbated_img[:, :, y: y + adv_dims[1],
                     x: x + adv_dims[0]]

        # write image for watching epoch evolution
        # write_image(perturbated_img, '/home/marco/Pictures/run/' +
        # str(epoch) + '.bmp')
        perturbated_img.to(device)

        # compute objective function components
        total_variance = lambda_attack * tv(noise_only)

        nps = get_nps_op(noise_only, triplets)
        avg_loss = generate_attack_avg_loss(model, loss_funct,
                                            perturbated_img,
                                            attack_class,
                                            transformations,
                                            opt)

        objective_function = total_variance + nps + avg_loss

        # write the adversarial image if a better candidate has been found
        if (objective_function.item() < self._best_object_function) and (
                total_variance.item() > 0) and (epoch > 1):
            self._best_object_function = objective_function
            generated_new = True
            write_image(noise_only, adv_out_file)
            # write complete image for reference
            write_image(perturbated_img, 'perturbated_image.bmp')

        print_epoch_stats(epoch, total_variance, nps, avg_loss,
                          objective_function, generated_new)

        optimizer.zero_grad()
        objective_function.backward()

        optimizer.step()

        plot_data.append([epoch, total_variance.data, nps, avg_loss.data,
                          objective_function.data])

    def attack_creation(self, model, loss_funct, img, noise, device,
                        lambda_attack,
                        epoch, optimizer, attack_class,
                        transformations,
                        triplets, opt,
                        adv_out_file, adv_coords,
                        adv_dims, mask, plot_data):

        optimizer.zero_grad()

        generated_new = False
        x, y = adv_coords[0], adv_coords[1]

        perturbated_img = img.clone().detach() * ~mask
        noise_conv = noise_forward(noise)
        masked_noise = (noise_conv * mask)
        perturbated_img = (perturbated_img + masked_noise)
        noise_only = perturbated_img[:, :, y: y + adv_dims[1],
                     x: x + adv_dims[0]]

        # write image for watching epoch evolution
        # write_image(perturbated_img, '/home/marco/Pictures/run/' +
        # str(epoch) + '.bmp')
        perturbated_img.to(device)

        # compute objective function components
        total_variance = lambda_attack * tv(noise_only)
        nps = get_nps_op(noise_only, triplets)
        avg_loss = generate_attack_avg_loss(model, loss_funct.loss_creation,
                                            perturbated_img,
                                            attack_class,
                                            transformations,
                                            opt, detailed_result=True,
                                            use_nms=False)

        objective_function = total_variance + nps + avg_loss

        # write the adversarial image if a better candidate has been found
        if (objective_function.item() < self._best_object_function) and (
                total_variance.item() > 0):
            self._best_object_function = objective_function.item()
            generated_new = True
            write_image(noise_only, adv_out_file)
            write_image(perturbated_img, 'perturbated_image.bmp')

        print_epoch_stats(epoch, total_variance, nps, avg_loss,
                          objective_function, generated_new)

        objective_function.backward()

        optimizer.step()

        plot_data.append([epoch, total_variance.data, nps, avg_loss.data,
                          objective_function.data])
