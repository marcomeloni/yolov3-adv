import torch
import torch.nn.functional as F
import numpy as np
import random
import cv2
from utils.general import non_max_suppression
from adv.loss import *

# COCO Mean and STD, used to compute noise from gradient
# ORIG
NORM_MEAN = [0.49139968, 0.48215827, 0.44653124]
NORM_STD = [0.24703233, 0.24348505, 0.26158768]
# NORM_MEAN = [0.44653124, 0.48215827, 0.49139968]
# NORM_STD = [0.26158768, 0.24348505, 0.24703233]

# OK NORM_STD = [0.24903233, 0.24948505, 0.24958768]

TENSOR_MEANS, TENSOR_STD = torch.FloatTensor(NORM_MEAN)[:, None, None], \
                           torch.FloatTensor(NORM_STD)[:, None, None]


# Compute total variance
def tv(noise):
    a = torch.abs(torch.diff(noise, dim=1))
    b = torch.abs(torch.diff(noise, dim=2))
    result = torch.sum(torch.abs(a[:, :, 0:a.shape[2] - 1] -
                                 b[:, 0:b.shape[1] - 1, :]))
    return result


# from https://github.com/evtimovi/robust_physical_perturbations/blob/master/
# lisa-cnn-attack/utils.py
def get_print_triplets(cols, rows):
    '''
    Reads the printability triplets from the specified file
    and returns a numpy array of shape (num_triplets, cols, rows, nb_channels)
    where each triplet has been copied to create an array the size of the image
    :param cols: number of columns
    :param rows: number of rows
    :return: as described
    '''
    p = []

    # load the triplets and create an array of the speified size
    with open("adv/triplets.txt") as f:
        for line in f:
            triplet = line.split(",")
            tr = []
            for t in triplet:
                tr.append(float(t))
            # tr[0], tr[2] = tr[2], tr[0]
            p.append(tr)

    p = map(lambda x: [[x for _ in range(cols)] for __ in range(rows)], p)

    # slow conversion
    p = np.array(list(p))
    return p


def squared_difference(x, y):
    return (x - y) ** 2


# from https://github.com/evtimovi/robust_physical_perturbations/blob/master/
# lisa-cnn-attack/utils.py
def get_nps_op(noise, printable_colors):
    '''
    Computes the nps op for the provided noise and printable colors.
    !!!!Assumes that printable_colors has been expanded to a 4D array
    of shape (number_of_triplets, noise_img_rows, noise_img_cols, number_of_channels)
    by replicating the same printability triplet to fill an image
    :param noise: the noise (could be masked)
    :param printable_colors: the printable colors
    :return: an operation computing the
    '''

    noise_t = noise[0].clone().detach()
    noise_t = np.transpose(noise_t, (2, 1, 0))  # was 1, 2, 0
    printab_pixel_element_diff = squared_difference(noise_t, printable_colors)
    a = np.sum(printab_pixel_element_diff.numpy(), axis=3)
    printab_pixel_diff = np.sqrt(a)
    printab_reduce_prod = np.prod(printab_pixel_diff, axis=0)
    return np.sum(printab_reduce_prod)


def transform_image_torch(image, rot, shift_x, shift_y):
    dtype = torch.cuda.FloatTensor if torch.cuda.is_available() \
        else torch.FloatTensor
    image = rot_img(image, rot, shift_x, shift_y, dtype)
    return image


# calculates the average loss of the image transformations
def generate_attack_avg_loss(model, loss_funct, image, attack_class,
                             transformations, opt, detailed_result=False,
                             use_nms=True):
    if detailed_result:
        threshold = 0
    else:
        threshold = opt.conf_thres
    # first let's use the untransformed image
    pred = model(image, augment=opt.augment)[0]
    if use_nms:
        pred = non_max_suppression(pred, threshold, opt.iou_thres,
                                   opt.classes, opt.agnostic_nms,
                                   max_det=opt.max_det)
    loss_sum = loss_funct(pred, attack_class)

    # iterate thru transformations
    for t in transformations:
        image_t = transform_image_torch(image, t['rot'], t['x'], t['y'])
        pred = model(image_t, augment=opt.augment)[0]
        if use_nms:
            pred = non_max_suppression(pred, threshold, opt.iou_thres,
                                       opt.classes, opt.agnostic_nms,
                                       max_det=opt.max_det)
        loss = loss_funct(pred, attack_class)
        loss_sum = loss + loss_sum

    return loss_sum / (len(transformations) + 1)


# Generate image transformation parameters
def generate_img_transformations(imgx, imgy, n=10):
    params = []
    for i in range(n):
        y = random.uniform(-0.1, 0.1)
        x = random.uniform(-0.1, 0.1)
        rot = random.uniform(-0.15, 0.15)
        params.append({'x': x, 'y': y, 'rot': rot})
    return params


# convert image in model format (1, 3, x, y) to CV2 format
def convert_4dimg_to_xyc(image):
    if not torch.is_tensor(image):
        image_t = torch.Tensor(image)
    else:
        image_t = image.clone().detach()[0]
    image_t = torch.transpose(image_t, 0, 2)
    image_t = torch.transpose(image_t, 0, 1)
    # return with swapped channels BGR to RGB
    return image_t.numpy()[:, :, ::-1]


# writes image from [1, 3, x, y] format
def write_image(image, path):
    img_p = image * 255
    cv2.imwrite(path, convert_4dimg_to_xyc(img_p))


# convert gradient values to image values (uses dataset mean and standard dev
def noise_forward(noise):
    # Map noise values from [-infty,infty] to COCO min and max
    noise = (torch.tanh(noise) + 1 - 2 * TENSOR_MEANS) / (2 * TENSOR_STD)
    return noise


# rotation using torch, in order to retain gradients.
# From https://stackoverflow.com/questions/64197754/
# how-do-i-rotate-a-pytorch-image-tensor-around-its-center-in-a-way-that-supports
def get_rot_mat(theta, shift_x, shift_y):
    theta = torch.tensor(theta)
    return torch.tensor([[torch.cos(theta), -torch.sin(theta), shift_x],
                         [torch.sin(theta), torch.cos(theta), shift_y]])


def rot_img(x, theta, shift_x, shift_y, dtype):
    rot_mat = get_rot_mat(theta, shift_x, shift_y)[None, ...].type(dtype). \
        repeat(x.shape[0], 1, 1)
    grid = F.affine_grid(rot_mat, x.size()).type(dtype)
    x = F.grid_sample(x, grid)
    return x


def print_epoch_stats(epoch, total_variance, nps, avg_loss,
                      objective_function, generated_new):
    if generated_new:
        str_new = " - Generated new adversarial image!"
    else:
        str_new = ""
    print("Epoch: " + str(epoch) +
          " - TV: " + str(total_variance.data) +
          " - NPS: " + str(nps) +
          " - Avg Loss: " + str(avg_loss.data) +
          " - Obj: " + str(objective_function.data) +
          str_new)


def create_mask(img, adv_coords):
    x1, y1, x2, y2 = adv_coords
    mask = torch.zeros(img.shape, dtype=torch.bool)
    if len(img.shape) == 4:
        mask[:, :, y1:y2, x1:x2] = True
    else:
        mask[:, y1:y2, x1:x2] = True
    return mask
