import argparse
import time
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.nn as nn

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages, convert_to_detect_format
from utils.general import check_img_size, check_requirements, check_imshow, \
    apply_classifier, scale_coords, xyxy2xywh, \
    strip_optimizer, set_logging, increment_path, save_one_box
from utils.plots import colors, plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

from adv.attack import *

import matplotlib.pyplot as plt


# @torch.no_grad()
def detect(opt):
    global modelc
    source, weights, view_img, save_txt, imgsz, adv = \
        opt.source, opt.weights, opt.view_img, \
        opt.save_txt, opt.img_size, opt.adv

    # save inference images
    save_img = not opt.nosave and not source.endswith('.txt')
    webcam = source.isnumeric() or source.endswith('.txt') or \
             source.lower().startswith(
                 ('rtsp://', 'rtmp://', 'http://', 'https://'))

    # Directories
    # make dir
    save_dir = increment_path(Path(opt.project) / opt.name,
                              exist_ok=opt.exist_ok)  # increment run
    (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True,
                                                          exist_ok=True)

    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size
    names = model.module.names if hasattr(model,
                                          'module') \
        else model.names  # get class names
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(
            torch.load('weights/resnet101.pt', map_location=device)[
                'model']).to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = check_imshow()
        # set True to speed up constant image size inference
        cudnn.benchmark = True
        dataset = LoadStreams(source, img_size=imgsz, stride=stride)
    else:
        dataset = LoadImages(source, img_size=imgsz, stride=stride)

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(
            next(model.parameters())))  # run once
    t0 = time.time()
    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres,
                                   opt.classes, opt.agnostic_nms,
                                   max_det=opt.max_det)
        t2 = time_synchronized()
        print(pred)

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0, frame = path[i], f'{i}: ', im0s[
                    i].copy(), dataset.count
            else:
                p, s, im0, frame = path, '', im0s.copy(), getattr(dataset,
                                                                  'frame', 0)

            p = Path(p)  # to Path
            save_path = str(save_dir / p.name)  # img.jpg
            txt_path = str(save_dir / 'labels' / p.stem) + (
                '' if dataset.mode == 'image' else f'_{frame}')  # img.txt
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[
                [1, 0, 1, 0]]  # normalization gain whwh
            imc = im0.copy() if opt.save_crop else im0  # for opt.save_crop
            if len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4],
                                          im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                # Write results
                for *xyxy, conf, cls in reversed(det):
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(
                            torch.tensor(xyxy).view(1, 4)) / gn).view(
                            -1).tolist()  # normalized xywh
                        line = (cls, *xywh, conf) if opt.save_conf else (
                            cls, *xywh)  # label format
                        with open(txt_path + '.txt', 'a') as f:
                            f.write(('%g ' * len(line)).rstrip() % line + '\n')

                    if save_img or opt.save_crop or view_img:  # Add bbox to image
                        c = int(cls)  # integer class
                        label = None if opt.hide_labels \
                            else (names[c] if opt.hide_conf
                                  else f'{names[c]} {conf:.2f}')
                        plot_one_box(xyxy, im0, label=label,
                                     color=colors(c, True),
                                     line_thickness=opt.line_thickness)
                        if opt.save_crop:
                            save_one_box(xyxy, imc,
                                         file=save_dir / 'crops' / names[
                                             c] / f'{p.stem}.jpg', BGR=True)

            # Print time (inference + NMS)
            # print(f'{s}Done. ({t2 - t1:.3f}s)')

            # Stream results
            if view_img:
                cv2.imshow(str(p), im0)
                cv2.waitKey(1)  # 1 millisecond

            # Save results (image with detections)
            if save_img:
                if dataset.mode == 'image':
                    cv2.imwrite(save_path, im0)
                else:  # 'video' or 'stream'
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            # release previous video writer
                            vid_writer.release()
                        if vid_cap:  # video
                            fps = vid_cap.get(cv2.CAP_PROP_FPS)
                            w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                            h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        else:  # stream
                            fps, w, h = 30, im0.shape[1], im0.shape[0]
                            save_path += '.mp4'
                        vid_writer = cv2.VideoWriter(save_path,
                                                     cv2.VideoWriter_fourcc(
                                                         *'mp4v'), fps, (w, h))
                    vid_writer.write(im0)

    if save_txt or save_img:
        s = f"\n{len(list(save_dir.glob('labels/*.txt')))} labels saved " \
            f"to {save_dir / 'labels'}" if save_txt else ''
        print(f"Results saved to {save_dir}{s}")

    print(f'Done. ({time.time() - t0:.3f}s)')


def adversarial(opt):
    source, weights, view_img, save_txt, imgsz, adv, adv_attack, adv_lambda, \
    adv_max_epoch, adv_class, adv_box_x1, adv_box_y1, adv_box_x2, \
    adv_box_y2, adv_n_transf, adv_learning_rate, adv_out_file, \
    adv_sticker_width, adv_sticker_height = \
        opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size, \
        opt.adv, opt.adv_attack, opt.adv_lambda, opt.adv_max_epoch, \
        opt.adv_class, opt.adv_box_x1, opt.adv_box_y1, opt.adv_box_x2, \
        opt.adv_box_y2, opt.adv_n_transf, opt.adv_learning_rate, \
        opt.adv_out_file, opt.adv_sticker_width, opt.adv_sticker_height

    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size
    names = model.module.names if hasattr(model, 'module') \
        else model.names  # get class names
    if half:
        model.half()  # to FP16

    t0 = time.time()

    time_synchronized()

    # some parameters
    opt.conf_thres = 0.001
    epoch = 1  # start epoch
    lambda_attack = adv_lambda
    max_epoch = adv_max_epoch
    plot_data = []
    plot_title = ""

    # TO DO: check for attack class
    attack_class = adv_class

    if (adv_attack == 1) and (source == 'bottle.jpg'):
        source = "blank.jpg"

    # Set Dataloader
    dataset = LoadImages(source, img_size=imgsz, stride=stride)

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(
            next(model.parameters())))  # run once
    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0

        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # t2 = time_synchronized()

        orig_img_y, orig_img_x = im0s.shape[:2]
        res_img_y, res_img_x = img.shape[2:]

        # Attack specific variable initialization
        if adv_attack == 0:  # disappearance
            plot_title = "Disappearance Attack"
            loss_funct = loss_disappearance

            # coordinates of box where the adversarial image will be applied
            x_start = adv_box_x1
            y_start = adv_box_y1
            x_stop = adv_box_x2
            y_stop = adv_box_y2

            # computing adversarial image sizes
            r_x = res_img_x / orig_img_x
            r_y = res_img_y / orig_img_y

            adv_coords = (int(x_start * r_x), int(y_start * r_y),
                          int(x_stop * r_x), int(y_stop * r_y))

        else:  # creation
            plot_title = "Creation Attack"
            loss_funct = LossCreation()

            if source == "blank.jpg":
                h_img_width = int(orig_img_x / 2)
                h_img_height = int(orig_img_y / 2)

                h_sticker_width = int(adv_sticker_width / 2)
                h_sticker_height = int(adv_sticker_height / 2)

                adv_coords = (h_img_width - h_sticker_width,
                              h_img_height - h_sticker_height,
                              h_img_width + h_sticker_width,
                              h_img_height + h_sticker_height)
            else:
                x_start = adv_box_x1
                y_start = adv_box_y1
                x_stop = adv_box_x2
                y_stop = adv_box_y2

                # computing adversarial image sizes
                r_x = res_img_x / orig_img_x
                r_y = res_img_y / orig_img_y

                adv_coords = (int(x_start * r_x), int(y_start * r_y),
                              int(x_stop * r_x), int(y_stop * r_y))

        mask = create_mask(img[0], adv_coords)

        adv_dims = (adv_coords[2] - adv_coords[0],
                    adv_coords[3] - adv_coords[1],
                    )

        # generate and save transformations
        transformations = generate_img_transformations(res_img_x,
                                                       res_img_y,
                                                       adv_n_transf)

        triplets = get_print_triplets(adv_dims[1], adv_dims[0])

        # setting up variable that will contain the noise
        noise = nn.Parameter(torch.zeros(img.shape[0:]), requires_grad=True)

        # setting up optimizer
        optimizer = optim.Adam([noise], lr=adv_learning_rate)

        attack = PhyAttack()

        while epoch < max_epoch:

            if adv_attack == 0:
                attack.attack_disappearance(model, loss_funct, img, noise,
                                            device,
                                            lambda_attack,
                                            epoch, optimizer, attack_class,
                                            transformations,
                                            triplets, opt,
                                            adv_out_file, adv_coords,
                                            adv_dims, mask, plot_data)
            else:
                attack.attack_creation(model, loss_funct, img, noise, device,
                                       lambda_attack,
                                       epoch, optimizer, attack_class,
                                       transformations,
                                       triplets, opt,
                                       adv_out_file, adv_coords,
                                       adv_dims, mask, plot_data)

            epoch += 1

    print(f'Done. ({time.time() - t0:.3f}s)')

    plot_data = np.array(plot_data)
    plt.title(plot_title)
    plt.plot(plot_data[:, 0], plot_data[:, 1])
    plt.plot(plot_data[:, 0], plot_data[:, 2])
    plt.plot(plot_data[:, 0], plot_data[:, 3])
    plt.plot(plot_data[:, 0], plot_data[:, 4])
    plt.legend(["Total Variance", "NPS", "Avg Loss", "Objective Function"])
    plt.xlabel("Epochs")
    plt.savefig("plot.png")


def print_classes(opt):
    source, weights, view_img, save_txt, imgsz, adv = \
        opt.source, opt.weights, opt.view_img, \
        opt.save_txt, opt.img_size, opt.adv
    model = attempt_load(weights)  # load FP32 model
    names = model.module.names if hasattr(model,
                                          'module') \
        else model.names  # get class names
    for index, cls in enumerate(names):
        print(str(index) + " " + cls)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov3.pt',
                        help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='bottle.jpg',
                        help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640,
                        help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25,
                        help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45,
                        help='IOU threshold for NMS')
    parser.add_argument('--max-det', type=int, default=1000,
                        help='maximum number of detections per image')
    parser.add_argument('--device', default='',
                        help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true',
                        help='display results')
    parser.add_argument('--save-txt', action='store_true',
                        help='save results to *.txt')
    parser.add_argument('--save-conf', action='store_true',
                        help='save confidences in --save-txt labels')
    parser.add_argument('--save-crop', action='store_true',
                        help='save cropped prediction boxes')
    parser.add_argument('--nosave', action='store_true',
                        help='do not save images/videos')
    parser.add_argument('--classes', nargs='+', type=int,
                        help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true',
                        help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true',
                        help='augmented inference')
    parser.add_argument('--update', action='store_true',
                        help='update all models')
    parser.add_argument('--project', default='runs/detect',
                        help='save results to project/name')
    parser.add_argument('--name', default='exp',
                        help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true',
                        help='existing project/name ok, do not increment')
    parser.add_argument('--line-thickness', default=3, type=int,
                        help='bounding box thickness (pixels)')
    parser.add_argument('--hide-labels', default=False, action='store_true',
                        help='hide labels')
    parser.add_argument('--hide-conf', default=False, action='store_true',
                        help='hide confidences')
    parser.add_argument('--adv', default=False, action='store_true',
                        help='performs adversarial attack')
    parser.add_argument('--adv-attack', default=0, type=int,
                        help='0 = disappearance attack - 1 = creation attack')
    parser.add_argument('--adv-lambda', default=0.02, type=float,
                        help='lambda for the attack')
    parser.add_argument('--adv-max-epoch', default=500, type=int,
                        help='max number of epoch for the attack')
    parser.add_argument('--adv-class', default=39, type=int,
                        help='class for the attack (COCO dataset)')
    parser.add_argument('--adv-box-x1', default=320, type=int,
                        help='upper X for adversarial box '
                             '(disappearance only)')
    parser.add_argument('--adv-box-y1', default=560, type=int,
                        help='upper Y for adversarial box '
                             '(disappearance only)')
    parser.add_argument('--adv-box-x2', default=450, type=int,
                        help='lower X for adversarial box '
                             '(disappearance only)')
    parser.add_argument('--adv-box-y2', default=670, type=int,
                        help='lower Y for adversarial box '
                             '(disappearance only)')
    parser.add_argument('--adv-sticker-width', default=64, type=int,
                        help='width of the sticker (creation only)')
    parser.add_argument('--adv-sticker-height', default=64, type=int,
                        help='Height of the sticker (creation only)')
    parser.add_argument('--adv-n-transf', default=5, type=int,
                        help='number of transformations in every epoch')
    parser.add_argument('--adv-learning-rate', default=0.01, type=float,
                        help='optimizer learning rate')
    parser.add_argument('--adv-out-file', default='attack.bmp', type=str,
                        help='lambda for the attack')
    parser.add_argument('--print-classes', default=False, action='store_true',
                        help='prints number->class')

    opt = parser.parse_args()
    print(opt)
    check_requirements(exclude=('tensorboard', 'pycocotools', 'thop'))

    if opt.update:  # update all models (to fix SourceChangeWarning)
        for opt.weights in ['yolov3.pt', 'yolov3-spp.pt', 'yolov3-tiny.pt']:
            detect(opt=opt)
            strip_optimizer(opt.weights)
    elif opt.adv:
        adversarial(opt=opt)
    elif opt.print_classes:
        print_classes(opt=opt)
    else:
        detect(opt=opt)
